package com.thisismartin.example.dto;

/**
 * Created with IntelliJ IDEA.
 * User: martin.spasovski
 * Date: 6/3/13
 * Time: 2:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class CommentDTO {

    public CommentDTO() {
    }

    String author;

    String text;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "CommentDTO{" +
                "author='" + author + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
