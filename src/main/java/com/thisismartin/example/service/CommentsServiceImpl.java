package com.thisismartin.example.service;

import com.thisismartin.example.dto.CommentDTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: martin.spasovski
 * Date: 6/3/13
 * Time: 3:07 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Qualifier(value = "commentsService")
public class CommentsServiceImpl implements CommentsService {

    /**
     * The comments 'Repository'
     */
    private List<CommentDTO> comments = new ArrayList<CommentDTO>();

    @Override
    public List<CommentDTO> getAll() {
        return comments;
    }

    @Override
    public CommentDTO add(CommentDTO updated) {
        comments.add(updated);
        return updated;
    }
}

